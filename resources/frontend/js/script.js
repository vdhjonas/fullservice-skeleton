jQuery(function($){
	$(".fullservice.top-links-area").hover(function() {
		/* Stuff to do when the mouse enters the element */
        $('.fullservice.top-links-area .top-links-icon').parent().children("ul.links").addClass("show");
	}, function() {
		/* Stuff to do when the mouse leaves the element */
        if($('.fullservice.top-links-area .top-links-icon').parent().children("ul.links").hasClass("show")){
            $('.fullservice.top-links-area .top-links-icon').parent().children("ul.links").removeClass("show");
        }
	});

	if( $(".input-different-shipping input[type=checkbox]").prop('checked') !== true ){
		$('.checkoutcontainer').addClass('different-shipping-address');
	}

	$(".input-different-shipping input[type=checkbox]").on('change', function(event) {
		console.log( $(this).prop('checked') );
		if($(this).prop('checked') == true){
	        $('.checkoutcontainer').removeClass('different-shipping-address');
        } else {
        	$('.checkoutcontainer').addClass('different-shipping-address');
        }
	});
});