# Privacy Policy

The right to personal privacy of each visitor will be respected on our website. To that end, we collect and use information throughout our website only as disclosed in this Privacy Policy. This statement applies solely to information collected on this website.

The personal information you provide to us will be treated with the utmost care and confidentiality. This information will only be gathered to make sure that your shopping experience will occur as smoothly as possible.

Personal data is processed according to the Belgian National Law and the EU Directive 95/46 as of October 24th 1995.  
This law stipulates that the person or company collecting data must secure the authorization of the person whose personal details are involved, that the data must be relevant, accurate and correct and that the data must be collected for specific, clear and lawful purposes. The person whose personal details are involved, must have access to this data and has the right to edit the information therein.

If you are not a registered user, you can browse our website anonymously as a Guest. Personal information of Guests will not be collected. However, you may not be able to use all services and features that we offer to  registered users.

When you make an online purchase, the following information is necessary:

1.  E-mail address
2.  First name and last name
3.  Billing address
4.  Shipping address
5.  Telephone number

We use our customers' email address to confirm orders and update order information. Upon request, we will add your e-mail address to our email newsletter to update you special promotions or new product arrivals. The choice of receiving these notices and product-related emails is yours. You can subscribe or unsubscribe at anytime. The telephone number enables our customer service to contact you when information should be necessary.

We guarantee that we do not sell, trade or rent personal information such as your name, email address or purchase history.

We use cookies to recognize you when you return to our shop, personalise your  shopping experience and maintain the content of your shopping bag. Cookies are small information files  stored on your hard drive and are automatically accepted by most browsers. You can change the options of  your browser to be notified of new cookies being set, delete old cookies or disable them altogether. Please  note that you may not be able to use all features and services offered on our shop when cookies are disabled.

We automatically collect anonymous data relating to your online session to gather statistical  information on our server’s usage and our overall shopping sessions . This may  include but is not limited to your browser type and version, your operating system and your IP address and/or domain name.

Your personal data may also be disclosed to any person or persons pursuant to any statutory or contractual obligations or as required by court of law, provided such person or persons shall be able to prove the required  right/authority to access such information.

You may wish to verify (“right of access”) or update (“right of correction”) any personal information that you  have submitted to us.

You can do so by logging in to your account on the shop and click on the link “My account”. You will then be able to view and/or update your personal  information including your name and email, shipping and  billing addresses and save email preferences.

Alternatively, you may prefer to contact our Customer Service team for help, support or enquiries on how you  can exercise these rights.

## Credit card payments

Ogone uses the Gemaco website and the online forms of the payment page only to collect and transmit data which is necessary to process your payments. This is for example the amount you have to pay, the currency and payment method you use to pay etc. Additional information, the merchant needs to be able to process your order, is collected (e.g. details on the order, delivery and billing address etc.). This data is stored for each transaction in order to be able to process the transaction and if necessary to check it. For fraud prevention reasons, Ogone is furthermore storing the IP-address of the computer, which was used to place the transaction.

Gemaco/Ogone does not collect any sensitive personal information, in particular with regard to race, religion or political affiliation.

Information collected by Gemaco/Ogone is used to execute the entire online payment transaction process, which was instructed by the consumer between him and the merchant.

Data collected by Ogone is stored for an adequate time to process the online payment/fulfill the contract between Gemaco and Ogone. Highest security standards are applied on data storage, all compliant to Payment Card Industry Data Security Standard (PCI DSS).

Gemaco/Ogone will not sell or rent private data to third parties. Furthermore, Gemaco/Ogone will not share private data with third parties, unless it is necessary to fulfill the contract/transaction you have with Gemaco/Ogone, or you have given us your allowance. Situations when your data is transferred to third parties are the following:

Ogone may share data with certain trusted partners, whose contribution is necessary to perform the payment process in the context of technical partnerships. Data might for example be shared with the post to send out invoices, the Visa, MasterCard, American Express... processors to process your payment to the merchants or to service providers to undertake analyses on behalf of Ogone.

These third parties are forbidden to use your personal information for any purpose other than the provision of these services to Ogone, and are obliged to keep your information confidential.

## Disclaimer

We reserve the right to amend this Privacy Policy at any time and will place any such amendments on this page. This Privacy Policy is not intended to, nor does it, create any contractual rights whatsoever or any other legal rights, nor does it create any obligations on the Company in respect of any other party or on behalf of any party.