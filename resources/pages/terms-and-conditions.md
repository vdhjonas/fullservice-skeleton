# Terms & Conditions

All prices, specifications and terms of sales are subject to change without notice. All orders are accepted subject to availability of merchandise.

Our standard delivery terms apply on all accepted orders. Changes to orders may incur additional charges, and your delivery date may be affected. Gemaco on behalf of Toyota will do its utmost to deliver your ordered goods as soon as possible. Once your order has been submitted, you will get a confirmation of your order by email.

If some items should not be available after you placed your order, a contact person of the Toyota Promoshop will contact you asap and inform you about the estimated date of delivery of the new stock. You may wish to select an alternative item if this delivery date for one or more items does not suit you.

## Terms of Sale

All prices are ex works Belgium but do not import taxes if they would apply. If available, a VAT number is necessary with each order to avoid VAT charges.

All orders will be debited in euro from your chosen credit card.

Prices and specifications are subject to change without prior notice. All measurements are approximate and are given as a guidance only.

Import taxes/ levies are not included in our prices and might be charged by your customs office separately. Please check with your customs office if import taxes/levies occur as these will have to be paid entirely by yourself. Gemaco NV will not be responsible and will not pay for any import taxes/levies that might be charged.

## Delivery

Items in stock will be shipped within 5 working days after receipt of our order confirmation and after payment. Standard delivery services will be used. If you choose a non-stocked item, the time of delivery might be longer but you will notified by email on the timing. All online orders must be shipped, local pickup is not available.

All items are subject to availability. Where an item is not currently in stock and a due date is quoted, this is given in good faith based on information from our manufacturer. These estimated due dates are not guaranteed and are subject to change.

## 100% Customer Satisfaction

We will do our utmost to fulfill your orders in a timely manner with most care. Should you receive a faulty or damaged item, please email us so we can arrange a replacement item for you. Faulty or damaged items need to be sent back to Gemaco within 14 days after delivery, stating the reasons for return and preferably in the original packaging.

If replacements are not available we will gladly offer you an alternative item or reimburse you. If a parcel is damaged at arrival, caused by the delivering company/freight forwarder, you can refuse the shipment, or sign for the package stating the degree of damage. All original packing material of damaged goods should be retained for any claim eligibilities.

If you need to return goods please contact us. Return freight must be pre-paid by the sender. The original shipping fee will be deducted from the reimbursement amount. Returned items that are used, altered, washed, worn, in poor condition or aren’t returned in their original packaging containing all labels, instructions or are missing certain items will not receive a credit or refund.

Returns of personalized items or special requests are only accepted if the cause of defect is due to the manufacturing process.

Order cancellations are subject to a cancellation fee, which may vary. Factory order cancellations for customized orders and non-stock items will not be granted and will have to be paid in full.

## Need a Custom Order?

Some special events or large campaigns will need customized/locally developed product solutions, so please contact us so we can help you with your requests.

## Item Use

Each item comes with illustrated pictures and a description.

This website is property of Gemaco. Access or usage of this website is subjected to our terms and conditions. When placing an order you will be asked to accept these terms and conditions.

## How to order?

1.  Choose your item by clicking on the picture or the product description.
2.  Enter quantity and click the Add To Cart button.
3.  Click on View Cart to get an overview of the selected items. Click on Continue Shopping to add more items or click on the Checkoutbutton to finalize your order.
4.  When confirming your order please make sure that billing and shipping address are correct (or change them if needed).
5.  Before finalizing the order you will have to accept the terms and conditions.
6.  Click on the Place Order button to confirm your order.
7.  You will receive an order confirmation by email.