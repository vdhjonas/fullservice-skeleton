/**
 *
 * Navigate in terminal to this project, type 'gulp' and enter to compile files.
 * Type 'gulp -- production' to minify the css and javascript when you're done and it's ready for production.
 * 
 *
 * Type 'gulp watch' to watch the files while you're busy with the frontend, 
    *
 * After everytime you save the scss or javascript file you will see a notification top right of your screen that it's compiled.
    * With browsersync it will open a new tab in your current browser with a different url (https://localhost:3000). it injects the css change after it's compiled
 * so you don't need to reload your page everytime. For javascript files it reloads the page. 
        *
 * You can also share the external url to colleagues sharing the same network (synchronised browsing doesn't work perfect but that's not the purpose for this gulpfile).
 */

/*=============================================
=            Only edit this value.            =
=============================================*/

var project = 'project',

/*=============================================
=       Don't edit anything below.            =
=============================================*/

mix = require('laravel-mix'),
fs = require('fs'),
xml2js = require('xml2js'),
parser = new xml2js.Parser(),
config_xml = fs.readFileSync('public/app/etc/config.xml'),
local_domain,
resource_path = 'resources/frontend',
skin_css_path = 'public/skin/frontend/smartwave/project/css',
skin_js_path = 'public/skin/frontend/smartwave/project/js';


parser.parseString(config_xml, (err, result) => {
    local_domain = result.config.global[0].limesoda[0].environments[0].local[0].variables[0].domain[0];
})

mix
    .disableNotifications()

    .js(`${resource_path}/js/script.js`, `${skin_js_path}/project.js`)
    .sass(`${resource_path}/sass/general.scss`, `${skin_css_path}/project.css`)

    /*----------  Customization for the email templates  ----------*/
    // email-inline.css : Magento will add these css rules in the style attribute in HTML elements
    // email-non-inline.css : Will come in a <style> element in the <head> section of the emails
    .sass(`${resource_path}/sass/email/email-inline.scss`, 'public/skin/adminhtml/default/default/css/email-inline.css')
    .sass(`${resource_path}/sass/email/email-non-inline.scss`, 'public/skin/adminhtml/default/default/css/email-non-inline.css')
    .copy('public/skin/adminhtml/default/default/css/email-inline.css', `${skin_css_path}/email-inline.css`)
    .copy('public/skin/adminhtml/default/default/css/email-non-inline.css', `${skin_css_path}/email-non-inline.css`)

    .browserSync({
        /*----------  setting proxy for live reloading - required ----------*/
        proxy: `https://${local_domain}`,
        files: [`${skin_css_path}/project.css`, `${skin_js_path}/project.js`]
    });
