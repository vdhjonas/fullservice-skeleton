
# README

The steps that are necessary to get your webshop locally up and running.

## How do I get set up?

* [View our youtube video](https://bitbucket.org/tutorials/markdowndemo)
* Clone repository with Tower or SourceTree
* Track and checkout on 'develop' branch
* Enable Git Flow

* Fix it on vagrant
* Add database
* Run composer install in terminal
* Run yarn in terminal

#### Deployment instructions

* Go to Envoyer.io (login in 1Password)
* Find project, if project doesn't exist [try setting it up yourself](https://meetunit.atlassian.net/wiki/spaces/NS/pages/35684361/Deployment+setup).
* Press red button
* Done

### Contribution guidelines

* Please get information from Uni-t E-commerce developers If you're new, not informed or unfamiliar with contributing code on projects.
* Get familiar with the [FullService-Cli](https://bitbucket.org/uni_t/fullservice-cli) & [terminal commands](https://bitbucket.org/uni_t/terminalcommands), more is explained in the readme files.

#### Most used commands (quicker when aliassing these)
1) fullservice modman:generate --dir="src" > modman
When adding files in project package src directory make sure to run this command in terminal to regenerate modman file

2) composer run-script post-install-cmd -vvv -- --redeploy
Afterwards navigate to project root (cd ../../../) and run this command to symlink the file. 
You'll see at the git workspace gitignore file & modman file are now edited, commit these files and proceed programming. ;)

sidenote: Also use this command when missing files after pulling commits from other developers that have committed files in the project package. It means these files are not yet symlinked on your local machine from project package to the public directory.

3) magerun ls:env:configure [local|development|staging|production]
Currently we have 4 environments, synchronizing the system configuration of the shop on all environments can be quite a chore doing it manually. That's why we use LimeSoda EnvironmentConfiguration, [read for more information.](https://github.com/LimeSoda/LimeSoda_EnvironmentConfiguration).

4) npm run dev & npm run watch
For compiling Sass & Javascript files we use Laravel Mix, which compiles 80% of most use cases but very simple to use. 
All you basically have to do is have node installed, npm installed, yarn installed, keep those up-to-date time to time. 

Run 'npm run dev' will start compiling the files you'll find in resources/frontend/sass & -js to skin/frontend/smartwave/project/css/project.css and -js/project.js.
Running 'npm run watch' you'll notice a node.js server starts running and it opens a new tab localhost:3000, it does the same as 'run dev' but now watches the compiled files and injects css changes directly to localhost:3000. This is called browsersync. when editing javascript files it refreshes the page.

## Sidenotes

### Purpose of fullservice-skeleton

With the fullservice-cli command `fullservice boilerplate:install` this repository will be cloned into your empty directory. It contains the bare minimum on files (boilerplate) for every new full service setup. The cleaner, the better. 

Once in a while new you might want to update the magento and fullservice package version in composer.json for the most optimal result.